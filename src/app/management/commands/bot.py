from django.conf import settings
from django.core.management import BaseCommand

from telebot import TeleBot
from telebot.types import ReplyKeyboardMarkup, KeyboardButton

from app.internal.models.admin_user import AdminUser

bot = TeleBot(settings.TELEGRAM_BOT_API_KEY)


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        bot.enable_save_next_step_handlers(delay=2)
        bot.load_next_step_handlers()
        bot.infinity_polling()


@bot.message_handler(commands=['start'])
def start(message):
    first_name = message.from_user.first_name
    last_name = message.from_user.last_name
    username = message.from_user.username

    AdminUser.objects.get_or_create(
        first_name=first_name,
        last_name=last_name,
        username=username,
    )

    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    reg_button = KeyboardButton(
        text="/set_phone"
    )
    keyboard.add(reg_button)

    bot.send_message(
        message.chat.id,
        f'Привет, {first_name} {last_name} ({username}) !',
        reply_markup=keyboard,
    )


@bot.message_handler(commands=['set_phone'])
def set_phone(message):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)

    reg_button = KeyboardButton(
        text="Отправить номер телефона",
        request_contact=True,
    )
    keyboard.add(reg_button)

    bot.send_message(
        message.chat.id,
        'Я хочу украсть твой номерок. '
        'Отправь мне его.',
        reply_markup=keyboard,
    )
    bot.register_next_step_handler(message, say_thanks)


@bot.message_handler(commands=['me'])
def get_me(message):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)

    try:
        user = AdminUser.objects.get(
            username=message.from_user.username
        )
    except:
        keyboard.add(
            KeyboardButton(
                text="/start"
            )
        )
        bot.send_message(
            message.chat.id,
            'Я тебя не знаю.',
            reply_markup=keyboard,
        )
        return



    if not user.phone_number:
        keyboard.add(
            KeyboardButton(
                text="/set_phone"
            )
        )
        bot.send_message(
            message.chat.id,
            'Сначала я хочу знать твой телефон.',
            reply_markup=keyboard
        )
    else:
        keyboard.add(
            KeyboardButton(
                text="/me"
            ),
        )
        bot.send_message(
            message.chat.id,
            'Что я знаю о тебе?\n\n'
            f'Username: {user.username}\n'
            f'First Name: {user.first_name}\n'
            f'Last Name: {user.last_name}\n'
            f'Phone Number: {user.phone_number}\n'
            'И ещё кое-что...😈',
            reply_markup=keyboard
        )


def say_thanks(message):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)

    try:
        user = AdminUser.objects.get(
            username=message.from_user.username
        )
    except:
        keyboard.add(
            KeyboardButton(
                text="/start"
            )
        )
        bot.send_message(
            message.chat.id,
            'Я тебя не знаю.',
            reply_markup=keyboard,
        )
        return

    user.phone_number = message.contact.phone_number
    user.save()

    keyboard.row(
        KeyboardButton(
            text="/me"
        ),
    )
    bot.send_message(
        message.chat.id,
        'Большое спасибо!\n'
        'Теперь я всё про тебя знаю 😈',
        reply_markup=keyboard
    )
