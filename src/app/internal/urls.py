from django.urls import path

from .transport.rest.handlers import get_me

urlpatterns = [
    path('me/<str:username>', get_me)
]
