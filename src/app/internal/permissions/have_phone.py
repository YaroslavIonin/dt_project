from rest_framework.permissions import BasePermission

from app.internal.models.admin_user import AdminUser


class IsHasPhone(BasePermission):
    message = 'Сначала я хочу знать твой номерок'

    def has_permission(self, request, view):
        username = view.kwargs.get("username")
        if not username:
            return False

        try:
            user = AdminUser.objects.get(
                username=username,
            )
        except AdminUser.DoesNotExist:
            self.message = 'Нет такого пользователя'
            return False

        return user.phone_number is not None

