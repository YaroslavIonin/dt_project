from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.internal.models.admin_user import AdminUser


@admin.register(AdminUser)
class AdminUserAdmin(UserAdmin):
    list_display = ('username', 'first_name', 'last_name')
    fieldsets = [
        (
            "Основная информация",
            {
                "fields": ['username', ('first_name', 'last_name')]

            }

        ),
        (
            "Для связи",
            {
                "fields": ['phone_number']
            }
        ),
    ]
