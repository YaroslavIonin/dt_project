from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes

from app.internal.models.admin_user import AdminUser
from app.internal.serializers import UserSummarySerializer

from app.internal.permissions import IsHasPhone


@api_view(['GET'])
@permission_classes([IsHasPhone])
def get_me(request, username):
    try:
        user = AdminUser.objects.get(
            username=username,
        )
    except AdminUser.DoesNotExist:
        return Response(
            {
                "error": "Такого пользователя нет"
            }
        )

    return Response(
        UserSummarySerializer(user).data
    )

