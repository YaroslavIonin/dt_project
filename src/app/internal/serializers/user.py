from rest_framework import serializers

from app.internal.models.admin_user import AdminUser


class UserSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminUser
        fields = (
            "username",
            "first_name",
            "last_name",
            "phone_number",
        )
